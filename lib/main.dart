import 'package:flutter/material.dart';
import 'cat.dart';
import 'cat_dao.dart';
import 'dog.dart';
import 'dog_dao.dart';

void main() async {
  
  var fido = Dog(id: 0, name: 'Fido', age: 35);
  var dido = Dog(id: 1, name: 'Dido', age: 20);
  await DogDao.insertDog(fido);
  await DogDao.insertDog(dido);

  print(await DogDao.dogs());

  fido = Dog(
    id: fido.id,
    name: fido.name,
    age: fido.age +7,
  );

  await DogDao.updateDog(fido);
  print(await DogDao.dogs());


  await DogDao.deleteDog(0);
  print(await DogDao.dogs());

  var ni = Cat(id: 0, name: 'ni', age: 2);
  var sang = Cat(id: 1, name: 'sang', age: 3);
  await CatDao.insertCat(ni);
  await CatDao.insertCat(sang);

  print(await CatDao.cats());

  ni = Cat(
    id: ni.id,
    name: ni.name,
    age: ni.age +7,
  );

  await CatDao.updateCat(ni);
  print(await CatDao.cats());


  await CatDao.deleteCat(0);
  print(await CatDao.cats());
}
