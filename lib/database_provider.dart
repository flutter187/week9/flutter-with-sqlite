import 'dart:async';
import 'package:flutter/material.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseProvider {
  static Future<Database>? _database;
  static Future<Database> get database {
    return _database ?? initDB();
  }

  static Future<Database> initDB() async {
    WidgetsFlutterBinding.ensureInitialized();
    _database = openDatabase(
      join(await getDatabasesPath(), 'kitty_and_doggie_database.db'),
      onCreate: (db, version) {
        db.execute(
          'CREATE TABLE dogs(id INTEGER PRIMARY KEY, name TEXT, age INTEGER)',
        );
        db.execute(
          'CREATE TABLE cats(id INTEGER PRIMARY KEY, name TEXT, age INTEGER)');
      },
      version: 1,
    );
    return _database as Future<Database>;
  }
}
